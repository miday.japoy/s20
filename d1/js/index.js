

/*Arrow function*/


// traditional function
// function ans(a){
// 	if(a < 10){
// 		return 'true';
// 	} else {
// 		return 'false';
// 	}
// };

// let answer = ans(5);
// console.log(answer);


// ternary operations ?: = if

const ans = (a) => (a < 10) ? 'true' : 'false';

let answer = ans(5);
console.log(answer);


// let mark = prompt('Enter your grade: ');
// let grade = (mark > 75) ? 'Passed' : 'Fail';
// console.log(`You ${grade} the exam.`);


/*Mini-Activity*/

// convert the following function into an arrow function and display the result in console.

	let numbers = [1, 2, 3, 4, 5];
	
	// let allValid = numbers.every(function(number){
	// 	return (number < 3)
	// });

	// let filterValid = numbers.filter(function(number){
	// 	return (number < 3)
	// })
	
	// let numberMap = numbers.map(function(number){
	// 	return number * number
	// });


	let allValid = numbers.every(numbers => numbers < 3);
	console.log(allValid);

	let filterValid = numbers.filter(numbers => numbers < 3);
	console.log(filterValid);

	let numberMap = numbers.map(numbers => numbers * 3);
	console.log(numberMap);


// to check if number is positive, negative or zero

	let num = 0;
		let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';
		console.log(`The number is ${result}`);



/* JSON */
//is a string
//JS object is an object//
//keys are surrounded with doucle qoutes


/* Syntax
	{
		"key":"value1",
		"key2":'value1'
	}

*/



const person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 20,
	"eyeColor" : "brown",
	"cars" : ["Toyota", "Honda"],
	"favoriteBooks" : {
		"title" : "When the fire Nation Attack",
		"author": "Nickelodean",
		"release": "2021"
	}
};

console.log(person);




/*Mini- Activity*/


const asset = [
{ 
	"id" : "JSON",
    "name" : "Jayson Uno",
    "description" : "The origin",
    "isAvailable" : true,
    "dateAdded" : "October 13, 2021"

},

{ 
	"id" : "JSON2",
    "name" : "Jayson Dos",
    "description" : "The Clone",
    "isAvailable" : false,
    "dateAdded" : "October 13, 2021"

}

];

console.log(asset);


/*XML format*/

/*

	<notes>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>
		<body> Don't forget me this weekend!! </body>
	</notes>
*/


// Stringify - method to conver Javascript aobject to JSON and vice versa


const cat = {
	"name" : "mashiro",
	"age" : 3,
	"weight" : 20
}


console.log(cat);


const catJSON = JSON.stringify(cat);
console.log(catJSON);

const json = catJSON
const cat1 = JSON.parse(json);
console.log(cat1)



