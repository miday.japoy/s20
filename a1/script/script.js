

/*
	Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

	Add the new course into the array and show the following alert:
		"You have created <nameOfCourse>. The price is <priceOfCourse>."

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
 */



let array = [];


const postCourse = (id, name, description, price, isActive) => {

	let course = {id, name, description, price, isActive};

	array.push(course);

	console.log(`You have created ${name}. The price is ${price}.`);

};


console.log(postCourse('courseA', 'JSON', 'toStringyfy', 5000, true));
console.log(postCourse('courseB', 'JSON', 'toStringyfy', 5000, true));



const idFinder = (array, courseID) => {
	const found = array.filter((a) => {
	return a.id === courseID;
})
	console.log(found);

};

console.log(idFinder(array, 'courseA'));


const deleteCourse = (array) => {
	array.pop();
	console.log(`Course was deleted.`);
}

console.log(deleteCourse(array));
console.log(array);

